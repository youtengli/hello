# 2025/03/06
- [ ] [報告簡報](https://docs.google.com/presentation/d/1WAjl9f1AR50CtUknmmyFJE3_I7wwXPVC3HK9Nrr-5pk/edit?usp=sharing)

# 2025/02/27
- [ ] [報告簡報](https://docs.google.com/presentation/d/1CMySEC6OdlVsp5F0rqFPCp-BfI5DtFO8Kfu203U2I_A/edit?usp=sharing)

# 2025/02/20
- [ ] [報告簡報](https://docs.google.com/presentation/d/1NAEiTh3tBZAu4TdiwZHUMDT71ix0deRghrzgavmIqOc/edit?usp=sharing)

# 2025/02/13
- [ ] [報告簡報](https://docs.google.com/presentation/d/1Jy7yoUBgmYgG8c-CVmiscGRBwhNTTHkwvBr26dklOeg/edit?usp=sharing)

# 2025/02/06
- [ ] [報告簡報](https://docs.google.com/presentation/d/1u0x_8cNYLxwigW9znqm84BCBTViK55rNWCsCY5LzXdg/edit?usp=sharing)

# 2025/01/21
- [ ] [報告簡報](https://docs.google.com/presentation/d/1yP1MvX7RxeMp9Igo1gTbWjbMO1C1kai9SojGckd75KA/edit?usp=sharing)

# 2025/01/16
- [ ] [報告簡報](https://docs.google.com/presentation/d/1_FDHZ2SHsbDRgV8O8GimvSNfnNErlCSIdHjX6zIYZmo/edit?usp=sharing)

# 2025/01/09
- [ ] [報告簡報](https://docs.google.com/presentation/d/1u6qhfu1LLOUWUpXo39poDppJ1cDX321g2b2WucgxUWA/edit?usp=sharing)

# 2024/12/26
- [ ] [報告簡報](https://docs.google.com/presentation/d/1m_vPxj3lb-gEnE050U-gkux3GzMbgBno7MpW4KjBYmU/edit?usp=sharing)

# 2024/12/19
- [ ] [報告簡報](https://docs.google.com/presentation/d/1tQFlrnz4j3DVgdtiPh-TTfHccPHFSaJogjQPBVaPYwg/edit?usp=sharing)

# 2024/12/12
- [ ] [報告簡報](https://docs.google.com/presentation/d/13aQMkXlc2yIPujeOqGkKkHSPdLvfuyOt1uncc0oRVpA/edit?usp=sharing)