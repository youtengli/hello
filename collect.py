import random
import numpy as np
from collections import Counter

# 文字陣列
colors = ['恭', '喜', '發', '財']

# 模擬的次數
num_simulations = 10000
# 需要湊齊數量
num_need = len(colors)
# 文字球數
num_ball = 4

# 隨機分發文字
box = []
for _ in range(num_ball * 4):
    box.append(colors[random.randint(0, 3)])
    
# 儲存每次模擬中湊齊 n 種顏色所需的抽取次數
draw_counts = []

# 模擬迴圈
for _ in range(num_simulations):
    random.shuffle(box)
    drawn_colors = set()
    draw_count = 0
    for ball in box:
        drawn_colors.add(ball)
        draw_count += 1
        if len(drawn_colors) == num_need:
            draw_counts.append(draw_count)
            break

# 計算在n次抽取內完成的機率
probabilities = {}
for i in range(num_need - 1 , num_need + 15):
    probabilities[i] = draw_counts.count(i) / num_simulations
print(f'主播數量為: {len(colors * num_ball)}, {num_need} 種文字')
# 輸出結果
count_distribution = Counter(draw_counts)
for k, v in probabilities.items():
    print(f'{k} 次完成機率: {v * 100 : .2f} %, 次數:{count_distribution[k]}')
# 計算各色機率
count_color = Counter(box)
for k in colors:
    print(f'{k}: {(count_color[k]/(num_ball*len(colors)))*100:.2f}%, 個數:{count_color[k]}')
# 輸出次數平均值和中位數
print(f'平均 {num_need} 種文字所需的抽取次數: {np.mean(draw_counts):.2f}')
print(f'中位數 {num_need} 種文字所需的抽取次數: {np.median(draw_counts):.2f}')
