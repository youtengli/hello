import numpy as np
import random
from collections import Counter

def simulate(num_colors, num_simulations):
    draw_counts = []
    for _ in range(num_simulations):
        drawn_colors = set()
        draw_count = 0
        while len(drawn_colors) < num_colors:
            color = random.randint(1, num_colors)
            drawn_colors.add(color)
            draw_count += 1
        draw_counts.append(draw_count)
    return draw_counts

num_colors = 4
num_simulations = 10000

results = simulate(num_colors, num_simulations)

# 計算平均次數和中位數
average_draw_count = np.mean(results)
median_draw_count = np.median(results)

print("取後放回，機率平均")
print("平均次數：", average_draw_count)
print("中位數：", median_draw_count)

# 列出每個抽取次數的出現次數
draw_count_counter = Counter(results)
for draw_count, frequency in sorted(draw_count_counter.items()):
    print("{} 次抽取: {} 次".format(draw_count, frequency))