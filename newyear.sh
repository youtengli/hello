#!/bin/bash

filename="phonelist.txt"
message="春節回娘家，別忘了我們！2024/12/31日前消費過的用戶，登入即贈50點，好禮等你拿！https://anhei.me"
encoded_message=$(echo "$message" | jq -s -R -r @uri)
cat "$filename" | while read line
do
  phone=$line
  curl "http://localhost/sms/api/api.php?code=12&phone=$phone&message=$encoded_message"
  sleep 1
done